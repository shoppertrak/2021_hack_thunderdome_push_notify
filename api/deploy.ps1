echo $PSScriptRoot

cd $PSScriptRoot\..\app

# $env:NODE_ENV = 'emart24'
# $env:NODE_DATASET_NAME = 'vwcloud_internal' 

ng build --configuration=production

echo $PSScriptRoot
cd $PSScriptRoot
npm run build

# ls

gcloud config set project sns-visionworkscloud-dev

gcloud builds submit --tag gcr.io/sns-visionworkscloud-dev/vw-pushnotification-app

gcloud run deploy vw-pushnotification-app --image gcr.io/sns-visionworkscloud-dev/vw-pushnotification-app --platform=managed --region=us-central1 --allow-unauthenticated --memory=4Gi --cpu=4 --max-instances=100 --timeout=15m --concurrency=80 --set-env-vars DATASET_NAME=vwcloud_internal

