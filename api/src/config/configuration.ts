export default () => ({
  port: parseInt(process.env.PORT, 10) || 8080,
  GCP_PROJECT: process.env.GCP_PROJECT || 'sns-visionworkscloud-dev',
  GCP_BQ_DB_LOCATION: 'US',
  GCP_BQ_DB_NAME: process.env.DATASET_NAME || 'vwcloud_internal',
});
