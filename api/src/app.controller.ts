import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('data')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/token')
  async getTrafficByHour(@Body() data: { token: string }) {
    return await this.appService.bqTokenInsert(data.token);
  }
}
