import { HttpException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

const { BigQuery } = require('@google-cloud/bigquery');

@Injectable()
export class AppService {
  projectId: string;
  datasetId: string;
  tableName: string;
  location: string;
  private client;
  constructor(private configService: ConfigService) {
    console.log(
      'GCP_PROJECT',
      this.configService.get<string>('GCP_PROJECT'),
      'GCP_BQ_DB_NAME',
      this.configService.get<string>('GCP_BQ_DB_NAME'),
    );
    this.projectId = configService.get<string>('GCP_PROJECT');
    this.datasetId = configService.get<string>('GCP_BQ_DB_NAME');
    this.location = this.configService.get<string>('GCP_BQ_DB_LOCATION');
    this.tableName = `${this.projectId}.${this.datasetId}.deviceTokens`;
    try {
      this.client = new BigQuery(this.projectId, 'config/keys/key.json');
    } catch (err) {
      console.log('Error in creating BigQuery Client. ', err);
      throw err;
    }
  }
  getHello(): string {
    return 'Hello World!';
  }

  async bqTokenInsert(token: string) {
    try {
      let row = await this.executeQuery(
        `SELECT count(token) as tokenCount FROM ${this.tableName} where token='${token}'`,
      );
      if (row && row[0]['tokenCount'] == 0) {
        await this.client
          .dataset(this.datasetId)
          .table('deviceTokens')
          .insert({ token: token });
      }
      return true;
    } catch (err) {
      throw new HttpException('Error in Querying. \n' + err.message, 500);
    }
  }

  async executeQuery(executableQuery) {
    try {
      const options = {
        query: executableQuery,
        location: this.location,
      };
      const [job] = await this.client.createQueryJob(options);
      const [rows] = await job.getQueryResults();
      return rows;
    } catch (error) {
      console.log('error');
    }
  }
}
