import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  constructor(
    private angularFireMessaging: AngularFireMessaging,
    private http: HttpClient
  ) {
    this.angularFireMessaging.messaging.subscribe((_messaging) => {
      _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    });
  }
  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        let postTokenApi = '/api/data/token';
        this.http
          .post<any[]>(postTokenApi, {
            token: token,
          })
          .subscribe((resp) => {});
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe((payload) => {
      console.log('new message received. ', payload);
      this.currentMessage.next(payload);
      this.showCustomNotification(payload);
    });
  }

  showCustomNotification(payload: any) {
    let notify_data = payload['notification'];
    let title = notify_data['title'];
    let options = {
      body: notify_data['body'],
      icon: './assets/icon.png',
      badge: './assets/icon.png',
      image: './assets/logo_resized.png',
    };
    //console.log('new message receobec', notify_data);
    let notify: Notification = new Notification(title, options);
    // notify.onclick = (event) => {
    //   event.preventDefault();
    //   window.location.href = 'https://sensormaticsolutions.atlassian.net/jira/';
    // };
  }
}
