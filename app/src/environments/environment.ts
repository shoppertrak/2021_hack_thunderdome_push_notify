// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDNlMvRNyF_zrqQy2iFowIBkhulAZe-yR4',
    authDomain: 'common-sns-ai-dev.firebaseapp.com',
    projectId: 'common-sns-ai-dev',
    storageBucket: 'common-sns-ai-dev.appspot.com',
    messagingSenderId: '804712925045',
    appId: '1:804712925045:web:c2cc8b1e017a2c0541b53c',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
