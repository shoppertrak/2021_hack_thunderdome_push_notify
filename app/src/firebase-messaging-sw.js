importScripts("https://www.gstatic.com/firebasejs/7.24.0/firebase-app.js");
importScripts(
  "https://www.gstatic.com/firebasejs/7.24.0/firebase-messaging.js"
);
if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyDNlMvRNyF_zrqQy2iFowIBkhulAZe-yR4",
    authDomain: "common-sns-ai-dev.firebaseapp.com",
    projectId: "common-sns-ai-dev",
    storageBucket: "common-sns-ai-dev.appspot.com",
    messagingSenderId: "804712925045",
    appId: "1:804712925045:web:c2cc8b1e017a2c0541b53c",
  });
} else {
  firebase.app(); // if already initialized, use that one
}
const messaging = firebase.messaging();
